ITEM_LIST = []


class Item(object):

    def __init__(self, name, description, grabbable=False):
        self._name = name
        self._description = description
        self._grabbable = grabbable
        ITEM_LIST.append(self)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def grabbable(self):
        return self._grabbable

    @grabbable.setter
    def grabbable(self, grabbable):
        self._grabbable = grabbable

    def __str__(self):
        return self.name

    # Events

    def on_interact(self, player):
        print("There\'s a time and a place for everything, but not now.")

    def on_examine(self):
        print(self.description)

    def on_grab(self, player):
        if self.grabbable:
            self.on_grab_successful(player)
        else:
            self.on_grab_fail(player)

    def on_grab_successful(self, player):
        player.add_item(self)
        print("You take the " + self.name + " and put it in your pocket.")

    def on_grab_fail(self, player):
        print("Attempting to pick up the " + self.name + " proved harder than you thought.")
