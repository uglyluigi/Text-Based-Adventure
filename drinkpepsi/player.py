from sys import exit as _term


class Player(object):

    def __init__(self, name):
        self.name = name
        self.dead = False
        self._inventory = []
        self._current_room = None

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        # Input validation
        if name == "Ajit Pai":
            print("You die instantly.")
            _term(0)
        else:
            self._name = name

    @property
    def dead(self):
        return self._dead

    @dead.setter
    def dead(self, value):
        self._dead = value

        if self._dead:
            # Death logic
            print("You arr ded. Not big sooprise.")

    @property
    def inventory(self):
        return self._inventory

    @property
    def current_room(self):
        return self._current_room

    @current_room.setter
    def current_room(self, room):
        print("You step into the " + room.name + ".")
        self._current_room = room
        room.on_enter(self)

    def pick_up(self, item):
        item.on_grab(self)
        self.current_room.remove_item(item)

    def add_item(self, item):
        self._inventory.append(item)

    def remove_item(self, item):
        self._inventory.remove(item)

    def has_item(self, item):
        return item in self.inventory
