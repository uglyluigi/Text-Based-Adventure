###########################################################################################
# Name: Brennan Forrest
# Date: 1/3/18
# Description: A text-based room-based-based adventure!
###########################################################################################

from time import sleep
from sys import exit as _term  # This prevents the interpreter from confusing exit(0) with the exit class
from room import Room, NORTH, SOUTH, EAST, WEST
from item import Item
from player import Player


# Rubric requirement 8-10

class Foyer(Room):

    def __init__(self, items):
        Room.__init__(self, "Foyer", items)
        self._room_id = 1

    def on_enter_first_time(self, player):
        print("It\'s dusty in here.")


class Den(Room):

    def __init__(self, items):
        Room.__init__(self, "Den", items)
        self._room_id = 2

    def on_enter_first_time(self, player):
        print("The floorboards creak under your feet.")


class Study(Room):

    def __init__(self, items):
        Room.__init__(self, "Study", items)
        self._room_id = 3

    def on_enter_first_time(self, player):
        print("That\'s a lot of books.")


class ManCave(Room):

    def __init__(self, items):
        Room.__init__(self, "Man Cave", items)
        self._room_id = 4

    def on_enter_first_time(self, player):
        pass

class SCP1025(Item):

    # This is a reference to the Secure Contain Protect horror collective. http://www.scp-wiki.net/scp-1025
    def __init__(self):
        Item.__init__(self, "book", "A dusty first-edition entitiled \"The Encyclopedia of Common Diseases.\"", True)

    def on_interact(self, player):
        print("You open the book to the table of contents.")
        sleep(2)
        print("You turn to the section titled \"Appendicitis\" and begin reading.")
        sleep(10)
        print("You\'re happy with what you\'ve learned.")
        sleep(4)
        print(
            "Pain begins to rise in your lower abdomen. It grows in intensity until an excrutiating flash of pain takes over your every sense.")
        sleep(4)
        print("You die of appendicitis.")
        sleep(3)
        death()


class Bookshelves(Item):

    def __init__(self):
        Item.__init__(self, "bookshelves",
                      "A mahogany bookshelf full of science fiction novels and encyclopedias. One of the books sticks out precariously.")
        self._has_book = True

    def on_grab_fail(self, player):
        print("You yank at the bookshelf. It topples over and falls on top of you, killing you instantly.")
        death()

    def on_interact(self, player):
        if self._has_book:
            print("You take the book from the bookshelf.")
            player.pick_up(book)
            self._has_book = False
            self.description = "A mahogany bookshelf full of science fiction novels and encyclopedias. It\'s now missing a book."


class Win(Item):

    def __init__(self):
        Item.__init__(self, "win", "Your free win!", True)

    def on_grab(self, player):
        print("You win!")
        print("Congrats!")
        _term(0)


# Game main

# Init items
ITEM_LIST = []

# Room 1 (Foyer)
table = Item("table", "An ornately carved wooden table that is caked with dust.")
chair = Item("chair", "A wooden chair that really ties the room together.")
gold_key = Item("key", "A shiny, antique golden key.", True)

# Room 2
rug = Item("rug", "A very expensive-looking Persian rug  with the price tag still on.")
fireplace = Item("fireplace", "A beautifully masoned fireplace containing very old soot.")

# Room 3
bookshelves = Bookshelves()
statue = Item("statue",
              "A large, marble statue with a plaque that reads \"The Java Developer.\" It\'s crying; probably using SE 6.")
desk = Item("desk", "A cocobolo desk fit for an office at Davis and Main.")  # Sick Better Call Saul reference
book = SCP1025()

# Room 4
brew_rig = Item("brew rig", "A rig used to brew beer. A plaque on the front reads \"Abita Brew Co.\"")
six_pack = Item("6-pack",
                "A box containing 6 recently brewed beers. Strangely fresh considering the apparent abesence of a resident.",
                True)
win = Win()


# Init Rooms
def create_rooms():
    global foyer
    global den
    global study
    global man_cave

    # Create the rooms
    foyer = Foyer([table, chair, gold_key])
    den = Den([rug, fireplace])
    study = Study([bookshelves, statue, desk, book])
    man_cave = ManCave([brew_rig, six_pack, win])

    # Add the exits
    foyer.add_exit(EAST, den)
    foyer.add_exit(SOUTH, study).set_key_item(gold_key).lock()
    man_cave.add_exit(NORTH, den)
    man_cave.add_exit(WEST, study)


create_rooms()

player = Player(raw_input("What is your name? "))


# Sneaky functions
def find_item(item):
    for n in ITEM_LIST:
        if item == n.name:
            return n


# Proxy function for the real one in the Player class
# Rubric requirement 4
def death():
    player.dead = True


def crack_open_a_cold_one_with_the_boys():
    print("The boys show up; you hand them both 2 cold ones.")
    sleep(3)
    print("You each crack open your cold ones.")
    sleep(3)

    n = 6
    while n > 0:
        print("You sip your cold one.")
        sleep(3.5)
        n -= 1

    sleep(3)
    print("You crack open your second cold one.")
    sleep(3)

    n = 6
    while n > 0:
        print("You sip your cold one.")
        sleep(3.5)
        n -= 1

    print(
        "The boys leave after thanking you for inviting them to drink a stranger's home-brewed alcohol in his own house.")
    drunk = 10000000000
    player.inventory.remove(six_pack)


# Main game loop
# Rubric requirement 3
player.current_room = foyer

while not player.dead:
    r_input = raw_input("> ")
    input = r_input.split(" ", 1)

    num_commands = len(input)

    if player.current_room == man_cave and six_pack in player.inventory and r_input == "crack open a cold one with the boys":
        crack_open_a_cold_one_with_the_boys()
        continue

    if num_commands == 1:  # Only 1 command
        if input[0] == "look":
            player.current_room.on_examine()
        elif input[0] == "die":
            death()
        elif input[0] in ["inventory", "inv"]:
            print("Your pockets contain: " + str([item.name for item in player.inventory]))
        elif input[0] in ["quit", "exit", "bye"]:
            print("See ya!")
            _term(0)
        elif input[0] in ["?", "help"]:
            print("You can look, use, go, and inv.")
        else:
            print("You're not quite sure how to " + input[0] + ".")
    elif num_commands == 2:
        item = find_item(input[1])
        item_found = item is not None

        if item_found:
            if input[0] == "look" and (item in player.inventory or item in player.current_room):
                item.on_examine()
            elif input[0] == "use":
                if (item.grabbable and item in player.inventory) or not item.grabbable:
                    item.on_interact(player)
                else:
                    print("You need to pick it up first.")
            elif input[0] in ["take", "grab", "get"]:
                if item not in player.inventory:
                    if item in player.current_room:
                        player.pick_up(item)
                else:
                    print("You take the " + item.name + " out of your pocket and put it back in.")
            continue

        if input[0] == "go":
            dict = {"north": NORTH, "south": SOUTH, "east": EAST, "west": WEST}

            try:
                direction = dict[input[1]]
            except KeyError:
                print("What kind of direction is that??")
                continue

            try:
                exit = player.current_room.exits[direction]
                exit.on_interact(player)
            except KeyError:
                print("There\'s no exit to the " + input[1] + ".")
            continue

        print("You can\'t quite remember how to {} the {}.".format(input[0], input[1]))
