class Room(object):

    # Rubric requirement 8
    def __init__(self, room_name, items=[]):
        self._room_name = room_name
        self._entered_before = False
        self._room_id = -1
        self._items = items
        self._exits = {}  # Stores the exits and their corresponding directions

    # Rubric requirement 9
    @property
    def items(self):
        return self._items

    @property
    def name(self):
        return self._room_name

    @property
    def exits(self):
        return self._exits

    def add_item(self, item):
        self._items += item

    def remove_item(self, item):
        self._items.remove(item)

    def has_item(self, item):
        return item in self._items

    # Adds an exit to the list of exits in the class instance.
    # Set reciprocate to true to also add a reciprocal exit.
    # Set it to false to produce a one-way exit.
    def add_exit(self, direction, dest_room, reciprocate=True):
        exit = Exit(direction, self, dest_room)
        self.add_exit_object(exit)

        if reciprocate:
            reciprocal_exit = Exit(direction.reciprocal, dest_room, self)
            dest_room.add_exit_object(reciprocal_exit)
            exit.set_reciprocal(reciprocal_exit)

        return exit

    def add_exit_object(self, exit):
        self._exits.update({exit.direction: exit})

    # Events. Usually overridden

    def on_enter(self, player):
        if not self._entered_before:
            self.on_enter_first_time(player)
            self._entered_before = True
        else:
            self.on_enter_nth_time(player)

    def on_enter_first_time(self, player):
        pass

    def on_enter_nth_time(self, player):
        pass

    def on_examine(self):
        print(str(self))

    # Overrides
    # Rubric requirement 10
    def __str__(self):
        exit_list = list(self.exits.keys())

        s = "There are exits on the " + str([str(exit) for exit in exit_list]) + " walls of this room.\n"
        s += "The " + self.name + " contains: " + str([item.name for item in self.items])

        return s

    # This sweetens the syntax a little more, so when I need to determine if an item is in a room,
    # I can say "item in room" instead of "item in room.items". I don't do it above because that's confusing.
    # As a Java user learning python, this is so cool. Maybe when Java 11 comes out in 50 years, Oracle will add op overloads.
    def __contains__(self, value):
        return value in self.items




class Direction(object):

    def __init__(self, value):
        self._value = value

    @property
    def reciprocal(self):
        return {NORTH: SOUTH, EAST: WEST, SOUTH: NORTH, WEST: EAST}[self]

    @property
    def value(self):
        return self._value

    def __str__(self):
        return {NORTH: "north", EAST: "east", SOUTH: "south", WEST: "west"}[self]


NORTH = Direction(0)
EAST = Direction(1)
SOUTH = Direction(2)
WEST = Direction(3)


class Exit(object):

    def __init__(self, direction, source_room, dest_room):
        self._direction = direction
        self._source_room = source_room
        self._dest_room = dest_room
        self._locked = False
        self._key_item = None
        self._reciprocal = None

    @property
    def direction(self):
        return self._direction

    @property
    def source_room(self):
        return self._source_room

    @property
    def dest_room(self):
        return self._dest_room

    @property
    def locked(self):
        return self._locked

    def set_reciprocal(self, exit, recurse=True):
        self._reciprocal = exit

        if recurse:
            self._reciprocal.set_reciprocal(self, False)

    def set_key_item(self, item, reciprocate=True):
        self._key_item = item

        if self._reciprocal is not None and reciprocate:
            self._reciprocal.set_key_item(item, False)

        return self

    def lock(self, reciprocate=True):
        if not self.locked and self._key_item is not None:  # If the exit is going from unlocked -> locked
            self._locked = True

        if self._reciprocal is not None and reciprocate:
            self._reciprocal.lock(False)

        return self

    def unlock(self, reciprocate=True):
        if self.locked:
            self._locked = False

        if self._reciprocal is not None and reciprocate:
            self._reciprocal.unlock(False)

        return self

    # Events

    def on_interact(self, player):
        if self.locked:
            if self._key_item in player.inventory:
                print("You use your key to open the door.")
                self.unlock()
            else:
                print("The door is locked and you lack a key.")
                return

        print("The door opens.")
        if player.current_room == self.source_room:
            player.current_room = self.dest_room
        elif player.current_room == self.dest_room:
            player.current_room = self.source_room
